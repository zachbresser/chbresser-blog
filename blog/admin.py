from django.contrib import admin

from blog.models import Post, Comment

@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    """ Admin Model for blog.models.Post """
    list_display = ('id', 'pub_date', 'title', 'short_content', 'author')
    ordering = ['id']

@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    """ Admin model for blog.models.Comment """
    list_display = ('id', 'parent', 'author', 'short_content')
    ordering = ['id',]
