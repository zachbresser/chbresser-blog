""" DRF Serializers for Blog Models """
from django.contrib.auth.models import User
from rest_framework import serializers

from blog.models import Post, Comment

class UserSerializer(serializers.ModelSerializer):
    """ Serializer for Builtin User Model """
    class Meta:
        model = User
        fields = (
            'first_name',
            'last_name',
            'username', 
            'email',
        )

class PostSerializer(serializers.ModelSerializer):
    """ Serializer for Blog Model 'Post' """
    class Meta:
        model = Post
        fields = (
            'pub_date',
            'author',
            'title',
            'content',
            'edit_date',
        )

class CommentSerializer(serializers.ModelSerializer):
    """ Serializer for Blog Model 'Comment' """
    class Meta:
        model = Comment
        fields = (
            'parent',
            'author',
            'content',
        )
