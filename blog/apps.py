""" App config for django app 'blog' """
from django.apps import AppConfig


class BlogConfig(AppConfig):
    """ App config for 'blog' """
    name = 'blog'
