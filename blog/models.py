""" Models for django app 'blog' """
from django.db import models
from django.contrib.auth.models import User
from django.template.defaultfilters import truncatechars
from taggit.managers import TaggableManager

class Post(models.Model):
    """ ORM representation of Post model """
    pub_date = models.DateField(auto_now_add=True)
    author = models.ForeignKey(
        User,
        on_delete=models.SET_NULL, 
        verbose_name="post author",
        null=True,
        blank=True
    )
    title = models.CharField(max_length=50)
    content = models.TextField()
    edit_date = models.DateField(null=True, blank=True)
    tags = TaggableManager()

    @property
    def short_content(self):
        """ Returns truncated content """
        return truncatechars(self.content, 200)

    def __str__(self):
        """ String Representation of Post """
        return "{0} by: {1}".format(self.title, self.author.username)

    def __repr__(self):
        """ Representation of Post """
        return "class Post <\
        Pub Date: {0}, \
        Author: {1}, \
        Title: {2}, \
        Content: {3}, \
        Edit Date: {4} \
        Tags: {5}>".format(
            self.pub_date, 
            self.author.username,
            self.title,
            self.content,
            self.edit_date,
            repr(self.tags)
        )

class Comment(models.Model):
    parent = models.ForeignKey(Post, on_delete=models.CASCADE)
    author = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )
    content = models.TextField()

    @property
    def short_content(self):
        """ Returns truncated content """
        return truncatechars(self.content, 200)

    
    def __str__(self):
        """ String representation of a comment """
        return "{0}'s comment on Post: {1}".format(self.author.username, self.parent.title)
