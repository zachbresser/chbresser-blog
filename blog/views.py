from django.contrib.auth.models import User
from django.shortcuts import render
from django.views.generic import TemplateView
from rest_framework import viewsets

from blog.serializers import PostSerializer, CommentSerializer, UserSerializer
from blog.models import Post, Comment

class Index(TemplateView):
    template_name = 'blog/index.html'
    http_method_names = ['get', 'head']

###################################################################################################
########################################## API VIEWS ##############################################
###################################################################################################
class PostViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Posts to be viewed or edited
    """
    queryset = Post.objects.all()
    serializer_class = PostSerializer

class CommentViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Comments to be viewed or edited
    """
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer

class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Users to be viewed or edited
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
