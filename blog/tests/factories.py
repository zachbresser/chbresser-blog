""" Factory-Boy Model Factories for Django Testing Blog """
import datetime
import factory

from django.contrib.auth.models import User

from blog.models import Post, Comment

class UserFactory(factory.django.DjangoModelFactory):
    """ Factory for User model """
    class Meta:
        model = User

    first_name = 'test'
    last_name = 'test'
    email = 'test@test.test'
    username = 'testuser'

class PostFactory(factory.django.DjangoModelFactory):
    """ Factory for Post Model """
    class Meta:
        model = Project

    pub_date = datetime.date.today()
    author = factory.SubFactory(UserFactory)a
    title = 'test Post'
    content = 'this is a test post that is for testing'

class Comment(factory.django.DjangoModelFactory):
    """ Factory for Comment Model """
    class Meta:
        model = Project

    parent = factory.SubFactory(PostFactory)
    author = factory.SubFactory(UserFactory)
    content = 'This is a test comment that is for testing'
